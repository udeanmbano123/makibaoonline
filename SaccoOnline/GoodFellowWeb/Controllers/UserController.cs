﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodFellowWeb.DAO.security;
using GoodFellowWeb.Models;
using RestSharp;
using Newtonsoft.Json;
using System.Collections;
using PagedList;
using GwendNyadhi.Models;
using GwendNyathi.Models;
using System.Text;
using System.Threading.Tasks;
using GwendNyadhi.Repositories;
using GwendNyathi.DROID;

namespace GoodFellowWeb.Controllers
{
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    public class UserController : Controller
    {
    
        public ActionResult MyAccount()
        {
            ViewBag.Msisdn=System.Web.HttpContext.Current.Session["Msisdn"].ToString();
            ViewBag.NatId = System.Web.HttpContext.Current.Session["NatId"].ToString();
            ViewBag.CdsNumber = System.Web.HttpContext.Current.Session["CdsNumber"].ToString();
            ViewBag.OtherNames = System.Web.HttpContext.Current.Session["OtherNames"].ToString();
            ViewBag.Surname= System.Web.HttpContext.Current.Session["Surname"].ToString();
            return View();
        }
        public ActionResult Index()
        {
            SessionCompanies();
            return View();
        }
        public ActionResult PrimaryMarket()
        {

            return View();
        }
        public ActionResult SecondaryBuy()
        {

            return View();
        }
        public ActionResult SecondarySell()
        {

            return View();
        }
        public ActionResult Portfolio()
        {

            return View();
        }
        public ActionResult Events()
        {

            return View();
        }
        public ActionResult AccountStatement()
        {
            ViewBag.Criteria = CompaniesReport();



            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountStatement([Bind(Include = "From,To,company")] Posting registerreport)
        {
            if (ModelState.IsValid)
            {
                System.Web.HttpContext.Current.Session["From"] = registerreport.From;
                System.Web.HttpContext.Current.Session["To"] = registerreport.To;
                System.Web.HttpContext.Current.Session["Company"] = registerreport.company;
                System.Web.HttpContext.Current.Session["Total"] = "0";
                return Redirect("~/Reports/AccountStatement.aspx?From=" + registerreport.From.ToString("M/dd/yyyy") + "&To=" + registerreport.To.ToString("M/dd/yyyy") + "&company=" + registerreport.company);
            }
            return View();
        }
        public ActionResult AccruedIntesrest()
        {

            return View("AccruedStatement");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccruedIntesrest([Bind(Include = "From,To")] Posting registerreport)
        {
            if (ModelState.IsValid)
            {
                //return Redirect("~/Reporting/AccruedReport.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View("AccruedStatement");
        }

        public JsonResult Profiles()
        {
           
         
        
            List<Profile> profiles = new List<Profile>();

            profiles.Add(new Models.Profile() { Msisdn = System.Web.HttpContext.Current.Session["Msisdn"].ToString(), NatId = System.Web.HttpContext.Current.Session["NatId"].ToString(), CdsNumber = System.Web.HttpContext.Current.Session["CdsNumber"].ToString(), OtherNames = System.Web.HttpContext.Current.Session["OtherNames"].ToString(), Surname= System.Web.HttpContext.Current.Session["Surname"].ToString() });
            return Json(profiles, JsonRequestBehavior.AllowGet);
    }
        public JsonResult ProfilesCompany(string company)
        {



            List<ProfileCompany> profiles = new List<ProfileCompany>();

            profiles.Add(new Models.ProfileCompany() { Msisdn = System.Web.HttpContext.Current.Session["Msisdn"].ToString(), NatId = System.Web.HttpContext.Current.Session["NatId"].ToString(), CdsNumber = System.Web.HttpContext.Current.Session["CdsNumber"].ToString(), OtherNames = System.Web.HttpContext.Current.Session["OtherNames"].ToString(), Surname = System.Web.HttpContext.Current.Session["Surname"].ToString(),Price=MyPrice(company)});
            return Json(profiles, JsonRequestBehavior.AllowGet);
        }

        public  string MyPrice(string company)
        {
            string price = "0.00";

            //get the price
            try
            {
                var client = new RestClient("http://192.168.3.14:9011/GetCurrentPrice?company=" + company);
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                var validate = response.Content;
                validate = validate.Replace(@"""", "");
                price = validate;
            }
            catch (Exception)
            {

            }
            return price;
        }
        public JsonResult Portfolios(Portfolio Portfolio)
        {
            string responses = "";

            try
            {
                var client = new RestClient("http://192.168.3.14:9011/Enquiry?cdsnumber="+ System.Web.HttpContext.Current.Session["CdsNumber"].ToString() + "&phone="+ System.Web.HttpContext.Current.Session["Msisdn"] .ToString() + "&company="+Portfolio.Security);
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                var validate = response.Content;
                validate = validate.Replace(@"""", "");
                responses = validate;
            }
            catch (Exception)
            {

            }

           return Json(responses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Companies()
        {
           
            List<Companies> dataList = (List<Companies>)Session["Companies"];



            return Json(dataList.ToList(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult PrimaryBuy(Orders Orders)
        {

            var client = new RestClient("http://192.168.3.14/APIMAKIBA/Subscriber/PrimaryBuy?quantity=" + Orders.Amount + "&cdsnumber=" + System.Web.HttpContext.Current.Session["CdsNumber"].ToString() + "&isin=" + Orders.Security + "&mobile=" + System.Web.HttpContext.Current.Session["Msisdn"].ToString());
            var request = new RestRequest("", Method.GET);
           IRestResponse response = client.Execute(request);
           var validate = response.Content;

         

            return Json(validate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SecondaryBuys(Orders Orders)
        {

            var client = new RestClient("http://192.168.3.14/APIMAKIBA/Subscriber/SecondaryBuyOrder?cdsnumber=" + System.Web.HttpContext.Current.Session["CdsNumber"].ToString() + "&phone=" + System.Web.HttpContext.Current.Session["Msisdn"].ToString() + "&quantity=" + Orders.Amount+ "&company=" + Orders.Security + "&price=" + MyPrice(Orders.Security));
           var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            var validate = response.Content;



            return Json(validate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SecondarySells(Orders Orders)
        {

            var client = new RestClient("http://192.168.3.14/APIMAKIBA/Subscriber/SecondarySellOrder?cdsnumber=" + System.Web.HttpContext.Current.Session["CdsNumber"].ToString() + "&phone=" + System.Web.HttpContext.Current.Session["Msisdn"].ToString() + "&quantity=" + Orders.Amount + "&company=" + Orders.Security + "&price=" + MyPrice(Orders.Security));
          var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            var validate = response.Content;



            return Json(validate, JsonRequestBehavior.AllowGet);
        }
        public void SessionCompanies()
        {
            try
            {

                var client = new RestClient("https://rununu.cdsckenya.com/MarketCompanies");
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                var validate = response.Content;
                List<Companies> dataList = JsonConvert.DeserializeObject<List<Companies>>(validate);

                Session["Companies"] = dataList.ToList();

            }
            catch (Exception)
            {


            }

           
        }
        public List<SelectListItem> CompaniesReport()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            List<Companies> dataList = (List<Companies>)Session["Companies"];

            foreach (var p in dataList)
            {
                phy.Add(new SelectListItem { Text = p.Security_Description, Value = p.Issuer_code });

            }


            return phy;
        }
        public JsonResult GetEvents(Portfolio Portfolio)
        {
                var client = new RestClient("http://192.168.3.14/APIMAKIBA/Subscriber/getevents");
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                var validate = response.Content;
            List<ParaEvent> dataList = JsonConvert.DeserializeObject<List<ParaEvent>>(validate);



            return Json(dataList, JsonRequestBehavior.AllowGet);
        }


    }
}

