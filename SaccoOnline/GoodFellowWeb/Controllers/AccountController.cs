﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Web.Security;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Migrations;
using GoodFellowWeb.DAO;
using GoodFellowWeb.Models;
using GoodFellowWeb.DAO.security;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using RestSharp;
using GwendNyadhi.Models;

namespace GwendNyadhi.Controllers
{
    public class AccountController : Controller
    {
        private SBoardContext db = new SBoardContext();

        static int counter = 0;
        static object lockObj = new object();
        SBoardContext Context = new SBoardContext();
      public string  baseUrl = "http://192.168.3.14/APIMAKIBA/Subscriber/";

        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {

         
            if (ModelState.IsValid)
            {
                //model.Password = ComputeHash(model.Password, new SHA256CryptoServiceProvider());
                var user = logg(model.Username,model.Password).FirstOrDefault();
                if (user != null)
                {
                  var roles = new string[] { "User" };
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = 0;
                    serializeModel.FirstName = user.OtherNames;
                    serializeModel.LastName = user.Surname;
                    serializeModel.CDSNumber = user.CdsNumber;
                    serializeModel.roles = roles;
                    string role = "User";
                   string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.CdsNumber,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(60),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    System.Web.HttpCookie faCookie = new System.Web.HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);



                    if(roles.Contains(role))
                    {
                    
                       return Redirect("~/"+role+"/Index");
                    }
                 else
                    {
                        return RedirectToAction("Index", "Account");
                    }
                }
         
                ModelState.AddModelError("", "Incorrect username and/or password");
            }
            model.Username = "";
            model.Password = "";
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Account", null);
        }

        [AllowAnonymous]
        public ActionResult StartUp() {

            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/'); ;
            return Redirect(baseUrl);
           
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        //setting forgot passsword
        public ActionResult ForgotPassword(LoginViewModel model)
        {


            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(LoginViewModel model, string returnUrl = "")
        {
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Forg/{s}", Method.PUT);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s",model.Username.ToString().Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                validate = validate.Replace(@"""", "");
                string val = validate;
            if (val!= "Password was not saved")
            {
                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add(val);           
            }
            else
            {
                var mod = ModelState.First(c => c.Key == "Username");  // this
                mod.Value.Errors.Add("Member does not exist in our records");
            }

            return View(model);

        }
        public List<Profile> logg(string username, string pass)
        {
            List<Profile> me = new List<Profile>();
            try
            {
                var client = new RestClient(baseUrl);
                var request = new RestRequest("authuser?mobile=" + username + "&password=" + pass, Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                IRestResponse response = client.Execute(request);
                var validate = response.Content;

                List<Profile> dataList = JsonConvert.DeserializeObject<List<Profile>>(validate);

                if (dataList.Count > 0)
                {
                    me = dataList;
                    //set session variables
                    foreach (var p in dataList)
                    {
                        System.Web.HttpContext.Current.Session["Msisdn"] = p.Msisdn;
                        System.Web.HttpContext.Current.Session["NatId"] = p.NatId;
                        System.Web.HttpContext.Current.Session["CdsNumber"] = p.CdsNumber;
                        System.Web.HttpContext.Current.Session["OtherNames"] = p.OtherNames;
                        System.Web.HttpContext.Current.Session["Surname"] = p.Surname;
                    }

                    return me;
                }
            }
            catch (Exception)
            {

            }
          

            return me;
        }
      
      

    }
}