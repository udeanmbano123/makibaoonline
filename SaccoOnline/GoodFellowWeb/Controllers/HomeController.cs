﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace GoodFellowWeb.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public JsonResult Reg(Registration Registration)
        {
            string responses = "";

            try
            {
                var client = new RestClient("http://192.168.3.14:9011/NewClients?idnumber=" + Registration.idnumber + "&othername=" + Registration.othername + "&surname=" + Registration.surname + "&postalcode=" + Registration.postalcode + "&country=" + Registration.country + "&dob=" + Registration.dob + "&gender=" + Registration.gender + "&nationality=" + Registration.nationality + "&resident=" + Registration.resident + "&phone=" + Registration.phone);
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                var validate = response.Content;
                validate = validate.Replace(@"""", "");
                if (validate=="13")
                {
                    responses = "Account is already registered.";
                }
                if (validate == "1")
                {
                    responses = "Please provide correct details.";
                }
                if (validate == "0")
                {
                    responses = "You account has been created successfully.";
                }
        
            }
            catch (Exception)
            {

            }

            return Json(responses, JsonRequestBehavior.AllowGet);
        }
    }
}