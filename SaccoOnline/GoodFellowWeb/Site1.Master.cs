﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodFellowWeb.DAO.security;
using RestSharp;
using Newtonsoft.Json;
using System.Collections;
using PagedList;

using System.Text;
using System.Web.UI.WebControls;
using GoodFellowWeb.Models;

namespace GwendNyathi
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated == false)
                {

                    Response.Redirect("~/Account", false);        //write redirect
                    Context.ApplicationInstance.CompleteRequest(); // end response
                }
                else
                {

                }
            }
            catch (Exception)
            {


                Response.Redirect("~/Account", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest(); // end response
            }
        }
    }
}