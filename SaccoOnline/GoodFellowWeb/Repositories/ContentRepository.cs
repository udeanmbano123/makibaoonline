﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using GoodFellowWeb.DAO;
using GoodFellowWeb.Models;
using GwendNyathi.DROID;


namespace GwendNyadhi.Repositories
{
    public class ContentRepository
    {
        private readonly DROID3Entities db = new DROID3Entities();
        public int UploadImageInDataBase(HttpPostedFileBase file, ContentViewModel contentViewModel,int fID)
        {
            contentViewModel.Image = ConvertToBytes(file);
            contentViewModel.AccountID = fID;
            var Content = new GwendNyathi.DROID.Content
            {
                Title = contentViewModel.Title,
                 UserID =contentViewModel.AccountID,
                 Image = contentViewModel.Image
            };
            db.Contents.Add(Content);
            int i = db.SaveChanges();
            if (i == 1)
            {
                return 1;
            }
            else
            {
                return 0;
            }

        }

        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }
    }
}