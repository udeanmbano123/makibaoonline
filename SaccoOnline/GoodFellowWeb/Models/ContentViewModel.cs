﻿

using System.ComponentModel.DataAnnotations;

namespace GoodFellowWeb.Models
{
    public class ContentViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public byte[] Image { get; set; }
        public int AccountID { get; set; }
       
    }
}
