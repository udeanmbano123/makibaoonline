﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using GoodFellowWeb.Models;

namespace GoodFellowWeb.Models
{
    public class Content 
    {     [Key]
        public int ID { get; set; }
        public string Title { get; set; }

        public byte[] Image { get; set; }
        public int AccountID { get; set; }
        public virtual User User { get; set; }
    }
}