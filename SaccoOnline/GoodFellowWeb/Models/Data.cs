﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GwendNyadhi.Models
{
    public class Data
    {
        public Data()
        {
        }
        public DateTime? tranDate;
        public string CDSNumber;
        public string Company;
        public string TranId;
        public double? Shares;
        public double? RunningShares;
        public DateTime? TranDate
        {
            get { return this.tranDate; }
            set { this.tranDate = value; }
        }
        public string cDSNumber
        {
            get { return this.CDSNumber; }
            set { this.CDSNumber = value; }
        }
        public string company
        {
            get { return this.Company; }
            set { this.Company = value; }
        }
        public string tranid
        {
            get { return this.TranId; }
            set { this.TranId = value; }
        }
        public double? shares
        {
            get { return this.Shares; }
            set { this.Shares = value; }
        }
       
        public double? runningshares
        {
            get { return this.RunningShares; }
            set { this.RunningShares = value; }
        }
    }
}