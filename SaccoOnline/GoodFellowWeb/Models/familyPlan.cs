﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using GoodFellowWeb.Models;
using Newtonsoft.Json;


using GoodFellowWeb.DAO.security;
using System.Text;

namespace GwendNyathi.Models
{
    public class familyPlan
    {


        public List<FamC> ListAll(string s)
        {

            s= HttpContext.Current.User.Identity.Name;
            List<FamC> lst = new List<FamC>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("LifeCover/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamC>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        public string Add(FamC my)
        {
            string CustNumber = HttpContext.Current.User.Identity.Name; ;
        
           
            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("FamilyCat/{Amount}/{Category}/{CustomerNumber}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("Amount", enc(isN(my.Amount )));
            request.AddUrlSegment("Category", enc(isN(my.Category)));
            request.AddUrlSegment("CustomerNumber", enc(isN(CustNumber)));
  
            // request.AddUrlSegment("b", enc(isN(my.OPTION_1)));

            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
  
    }
