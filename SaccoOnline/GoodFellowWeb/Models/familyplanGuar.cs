﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using GwendNyadhi.Models;

namespace GwendNyathi.Models
{
    public class familyPlanGuar
    {
        public List<FamilyGuar> ListAll(string s)
        {
            List<FamilyGuar> lst = new List<FamilyGuar>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyGaran/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyGuar>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        public List<FamilyGuar> getem(string s)
        {
            List<FamilyGuar> lst = new List<FamilyGuar>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyGuaL/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s.ToString());
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyGuar>>(response.Content);


            }
            catch (Exception)
            {


            }
            return lst;
        }
        //Method for Adding an Employee
        public string Add(FamilyGuar my)
        {

            string CustNo = HttpContext.Current.User.Identity.Name; ;
            // my.CustNo = CustNo;
            var de = my.Names.Split(',')[0];
            var des = my.Names.Split(',')[1];
            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("FamilyGuaAdd/{a}/{b}/{c}/{d}/{e}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(de)));
            request.AddUrlSegment("b", enc(isN(my.Shares)));
            request.AddUrlSegment("c", enc(isN(my.Pledge)));
            request.AddUrlSegment("d", enc(isN(des)));
            request.AddUrlSegment("e", enc(isN(CustNo)));
           IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        //Method for Updating Employee record
        public int Update(FamilyGuar my)
        {
                  
            string CustNo = HttpContext.Current.User.Identity.Name;
            var de = my.Names.Split(',')[0];
            var des = my.Names.Split(',')[1];
            int i = 1;
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamilyGuaU/{a}/{b}/{c}/{d}/{e}/{f}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(de)));
            request.AddUrlSegment("b", enc(isN(my.Shares)));
            request.AddUrlSegment("c", enc(isN(my.Pledge)));
            request.AddUrlSegment("d", enc(isN(des)));
            request.AddUrlSegment("e", enc(isN(CustNo)));
            request.AddUrlSegment("f", enc(isN(my.id.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return i;
        }
        //Method for Deleting an Employee
        public string Delete(int ID)
        {
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamilyGuaR/{a}", Method.DELETE);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", ID.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
        public List<GD> ListAllGD(string s)
        {
            List<GD> lst = new List<GD>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("GDetails/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<GD>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
    }
}