﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace GoodFellowWeb.Models
{
    public class Products
    {
        public string id { get; set; }

        public string DisplayName { get; set; }
    }

    public class LoanA
    {
        [Required]
        [DisplayName("Loan Amount")]
        public decimal LoanAmount { get; set; }
        [Required]
        [DisplayName("Loan Product")]
        public string product { get; set; }
    }
    public partial class Payment
    {
       

       public string CustomerNumber { get; set; }
        [Required]
        public Nullable<decimal> Amount { get; set; }
        [Required]
        public string PaymentType { get; set; }
        [Required]
        public string MobileNumber { get; set; }
    }
    public  class Withdrawals
    {


        public string CustomerNumber { get; set; }
        [Required]
        public Nullable<decimal> SavingsAmount { get; set; }
        [Required]
        public Nullable<decimal> Amount { get; set; }

    }

    public class ShareTransfer
    {


        public string CustomerNumber { get; set; }
        [Required]
        public string ShareBalance { get; set; }
       public string TransferTo { get; set; }
        [Required]
        public decimal numbershares { get; set; }

    }
    public class Contact
    {
        [Required(ErrorMessage = "Please enter a name")]
        public string name { get; set; }
        [Required(ErrorMessage = "Please enter a valid email")]
        public string email { get; set; }
        [Required(ErrorMessage = "Please enter subject")]
        public string Subject { get; set;}
        [Required(ErrorMessage = "Please write something for us")]
        public string Message { get; set; }
    }
}