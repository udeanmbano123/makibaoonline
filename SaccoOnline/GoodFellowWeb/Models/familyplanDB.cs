﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using GoodFellowWeb.DAO.security;
namespace GwendNyadhi.Models
{
    public class familyPlanDB
    {
        public List<FamilyPlan> ListAll(string s)
        {
            List<FamilyPlan> lst = new List<FamilyPlan>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlans/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyPlan>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        public List<FamilyPlan> getem(string s)
        {
            List<FamilyPlan> lst = new List<FamilyPlan>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlans/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s.ToString());
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyPlan>>(response.Content);


            }
            catch (Exception)
            {


            }
            return lst;
        }
        //Method for Adding an Employee
        public string Add(FamilyPlan my)
        {

            string neme = isN(Convert.ToDateTime(my.Birth_Date).ToString("dd-MMM-yyyy"));

            string CustNo = HttpContext.Current.User.Identity.Name; ;
            my.CustNo = CustNo;
            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("FamilyPlan/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Full_Names)));
            request.AddUrlSegment("b", enc(isN(my.ID_Number_)));
            request.AddUrlSegment("c", enc(neme));
            request.AddUrlSegment("d", enc(isN(my.Genfer)));
            request.AddUrlSegment("e", enc(isN(my.PIN_Number)));
            request.AddUrlSegment("f", enc(isN(my.Occupation)));
            request.AddUrlSegment("g", enc(isN(my.Email)));
            request.AddUrlSegment("h", enc(isN(my.Mobile_)));
            request.AddUrlSegment("i", enc(isN(my.PBox)));
            request.AddUrlSegment("j", enc(isN(my.Town)));
            request.AddUrlSegment("k", enc(isN(my.Product)));
            request.AddUrlSegment("l", enc(isN(my.CustNo)));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        //Method for Updating Employee record
        public int Update(FamilyPlan my)
        {

            string neme = isN(Convert.ToDateTime(my.Birth_Date).ToString("dd-MMM-yyyy"));

            string CustNo = HttpContext.Current.User.Identity.Name; ;
            my.CustNo = CustNo;
            int i = 1;
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamilyPlanU/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}/{m}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Full_Names)));
            request.AddUrlSegment("b", enc(isN(my.ID_Number_)));
            request.AddUrlSegment("c", enc(neme));
            request.AddUrlSegment("d", enc(isN(my.Genfer)));
            request.AddUrlSegment("e", enc(isN(my.PIN_Number)));
            request.AddUrlSegment("f", enc(isN(my.Occupation)));
            request.AddUrlSegment("g", enc(isN(my.Email)));
            request.AddUrlSegment("h", enc(isN(my.Mobile_)));
            request.AddUrlSegment("i", enc(isN(my.PBox)));
            request.AddUrlSegment("j", enc(isN(my.Town)));
            request.AddUrlSegment("k", enc(isN(my.Product)));
            request.AddUrlSegment("l", my.famID.ToString());
            request.AddUrlSegment("m", enc(isN(my.CustNo)));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return i;
        }
        //Method for Deleting an Employee
        public string Delete(int ID)
        {
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamilyPlanR/{a}", Method.DELETE);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", ID.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}