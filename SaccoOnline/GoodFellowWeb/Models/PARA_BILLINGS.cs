﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GwendNyadhi.Models
{
    public class PARA_BILLINGS
    {
        public DateTime? TranDate { get; set; }
        public string CDSNumber { get; set; }
        public string Company { get; set; }
        public string TranId { get; set; }
        public decimal Shares { get; set; }
    }
}