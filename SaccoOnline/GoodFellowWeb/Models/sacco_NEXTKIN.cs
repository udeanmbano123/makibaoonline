﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodFellowWeb.Models
{
    public class sacco_NEXTKIN
    {
        public int id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string OtherNames { get; set; }
        public string PBox { get; set; }
        public string PostalCode { get; set; }
        public string ID_Number { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string CustomerNo { get; set; }
        public Nullable<decimal> Percentage { get; set; }
    }
    public class FamilyDependant
    {
        

        public int id { get; set; }
        public string FullNames { get; set; }
        public System.DateTime DOB { get; set; }
        public string Relationship { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PlanNo { get; set; }
    }
    public class FamilyPlanKIN
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string ID_Number { get; set; }
        public System.DateTime DOB { get; set; }
        public string Relationship { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PlanNo { get; set; }
    }
    public class FamilyProduct
    {
        public string CATEGORY { get; set; }
        public Nullable<double> OPTION_1 { get; set; }
        public Nullable<double> OPTION_2 { get; set; }
        public Nullable<double> OPTION_3 { get; set; }
        public Nullable<double> OPTION_4 { get; set; }
        public Nullable<double> OPTION_5 { get; set; }
        public int ID { get; set; }
    }
    public class FamilyPlan
    {
        public int famID { get; set; }
        public string Full_Names { get; set; }
        public string ID_Number_ { get; set; }
        [Required]
        public Nullable<System.DateTime> Birth_Date { get; set; }
        [DisplayName("Gender")]
        public string Genfer { get; set; }
        [DisplayName("PIN Number")]
        public string PIN_Number { get; set; }
        public string Occupation { get; set; }
        public string Email { get; set; }
        [DisplayName("Mobile")]
        public string Mobile_ { get; set; }
        public string PBox { get; set; }
        public string Town { get; set; }
        [Required]
        public string Product { get; set; }
        public string Status { get; set; }
        public string CustNo { get; set; }
    }
    public class FamilyGuar
    {
        public int id { get; set; }
        public string Names { get; set; }
        public string Pledge { get; set; }
        public string Shares { get; set; }
     }
}