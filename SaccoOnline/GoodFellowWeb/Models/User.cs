using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CompareObsolete = System.Web.Mvc.CompareAttribute;
namespace GoodFellowWeb.Models
{
   
    public  class User
    {
        [Key]
        public int UserId { get; set; }
        public string username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string MemberNumber { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public int LockCount { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

    }
    //[{"Msisdn":"254723274337","NatId":"22489422","CdsNumber":"001347109398860","OtherNames":"RAPHAEL","Surname":"OTIENO"}]
    public class Profile
    {
       
        public string Msisdn { get; set; }
        public string NatId { get; set; }
        public string CdsNumber { get; set; }
        public string OtherNames { get; set; }
        public string Surname { get; set; }
   
    }
    public class ProfileCompany
    {

        public string Msisdn { get; set; }
        public string NatId { get; set; }
        public string CdsNumber { get; set; }
        public string OtherNames { get; set; }
        public string Surname { get; set; }
        public string Price { get; set; }

    }

    public class Register
    {
  
        public string username { get; set; }
        [Display(Name = "Client Number")]
        public string clientnumber { get; set; }
        [Display(Name = "Password")]
        [Required]
        public string password { get; set; }
        [Display(Name = "ConfirmPassword")]
        [Required]
        [CompareObsolete("password", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirmpassword { get; set; }
        [Display(Name = "Surname")]
        [Required]
        public string surnameP { get; set; }
        [Display(Name = "Name")]
        [Required]
        public string nameP { get; set; }
        [Display(Name = "Gender")]
        [Required]
        public string gender { get; set; }
        [Display(Name = "Title")]
        [Required]
        public string title { get; set; }
        [Display(Name = "ID/Passport Number")]
        [Required]
        public string idnumber { get; set; }
        [Display(Name = "Address")]
        [Required]
        public string address { get; set; }
        [Display(Name = "Code")]
        [Required]
        public string code { get; set; }
        [Display(Name = "City")]
        [Required]
        public string town { get; set; }
        [Display(Name = "Email")]
        [Required]
        public string emailP { get; set; }
        [Display(Name = "Telephne")]
        [Required]
        public string telephone { get; set; }
        public string farmlocation { get; set; }
        public string crops { get; set; }
        public string livestock { get; set; }
        public string agriculture { get; set; }
        public string average { get; set; }
        public string employer { get; set; }
        public string workstation { get; set; }
        public string designation { get; set; }
        public string businessname { get; set; }
        public string legalstatus { get; set; }


    }
    public class ForgotP
    {

        [Display(Name = "Client Number")]
        [Required]
        public string clientnumber { get; set; }
      

    }
}
