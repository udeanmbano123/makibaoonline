﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace GoodFellowWeb.Models
{
    public class LoanStatus
    {
        public string ID { get; set; }
        public string STATUS { get; set; }

        public decimal AMOUNT { get; set; }

        public DateTime CREATED_DATE { get; set; }
    }

    public class LCounts
    {
        public string status { get; set; }
        public int numLoans { get; set; }
    }

    public class LoanApplication
    {
      
     
        [Required(ErrorMessage = "Will you be applying this loan with co-applicant?")]
        public string coapplicant { get; set; }
        [Required(ErrorMessage = "First Name is mandatory")]
        public string First { get; set; }
        [Required(ErrorMessage = "Last Name is mandatory")]
        public string Last { get; set; }
        [Required(ErrorMessage = "Date Of Birth is mandatory")]
        public DateTime DOB { get; set; }
        [Required(ErrorMessage = "Phone is mandatory")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Address is mandatory")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Do you rent or own?")]
        public string OwnRent { get; set; }
        [Required(ErrorMessage = "Monthly rent or Mortgage?")]
        public string MonthlyAmout { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [StringLength(100, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Desired loan amount")]
         public string Amount { get; set; }
        public string UsedFor { get; set; }
        [Required(ErrorMessage = "Savings Amount")]

        public decimal Savings { get; set; }
        [Required(ErrorMessage = "Job Start Date")]
        public DateTime JobStartDate { get; set; }
        [Required(ErrorMessage = "Job Title")]

        public string JobTitle
        {
            get; set;
        }
        [Required(ErrorMessage = "Employer name?")]
        public string EmployerName
        {
            get; set;
        }
        [Required(ErrorMessage = "How did you hear about us?")]
         public string Hear
        {
            get; set;
        }
        [Required]
        public Decimal AnnualIncome
        {

            get; set;
        }


        [Required(ErrorMessage = "PayBack period is required")]
        public int Tenor
        {
            get; set;
        }
        [Required(ErrorMessage = "Product is required")]
        public string Product
        {
            get; set;
        }

    }
}