﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GwendNyadhi.Models
{
    public class familyPlanKinDB
    {
        public List<FamilyPlanKIN> ListAll(string s)
        {
            List<FamilyPlanKIN> lst = new List<FamilyPlanKIN>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlanKINDD/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyPlanKIN>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        public List<FamilyPlanKIN> getem(int s)
        {
            List<FamilyPlanKIN> lst = new List<FamilyPlanKIN>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlanKINDDD/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s.ToString());
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyPlanKIN>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        //Method for Adding an Employee
        public string Add(FamilyPlanKIN my)
        {
            string CustNo = HttpContext.Current.User.Identity.Name; ;
            my.PlanNo = CustNo;
            string neme = isN(Convert.ToDateTime(my.DOB).ToString("dd-MMM-yyyy"));
            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("FamiyPlanKIN/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Name)));
            request.AddUrlSegment("b", enc(isN(my.ID_Number)));
            request.AddUrlSegment("c", enc(isN(neme)));
            request.AddUrlSegment("d", enc(isN(my.Relationship)));
            request.AddUrlSegment("e", enc(isN(my.Email)));
            request.AddUrlSegment("f", enc(isN(my.Phone)));
            request.AddUrlSegment("g", enc(isN(my.Address)));
            request.AddUrlSegment("h", enc(isN(my.PlanNo)));
           IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        //Method for Updating Employee record
        public int Update(FamilyPlanKIN my)
        {
            int i = 1;
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamiyPlanKINU/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Name)));
            request.AddUrlSegment("b", enc(isN(my.ID_Number)));
            request.AddUrlSegment("c", enc(isN(my.DOB.ToString("dd-MMM-yyyy"))));
            request.AddUrlSegment("d", enc(isN(my.Relationship)));
            request.AddUrlSegment("e", enc(isN(my.Email)));
            request.AddUrlSegment("f", enc(isN(my.Phone)));
            request.AddUrlSegment("g", enc(isN(my.Address)));
            request.AddUrlSegment("h", enc(isN(my.PlanNo)));
            request.AddUrlSegment("i", enc(isN(my.id.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return i;
        }
        //Method for Deleting an Employee
        public string Delete(int ID)
        {
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamiyPlanKIND/{a}", Method.DELETE);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", ID.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}