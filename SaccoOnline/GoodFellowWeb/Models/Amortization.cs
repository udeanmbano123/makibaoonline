﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GwendNyadhi.Models
{
    public class Amortization
    {

        public string LOANID { get; set; }
        public string PAYMENT_NO { get; set; }
        public string PAYMENT_DATE { get; set; }
        public string PRINCIPAL { get; set; }
        public string ADMIN_CHARGE { get; set; }
        public string PAYMENT { get; set; }
        public string CUMULATIVE_PRINCIPAL { get; set; }
        public string CUMULATIVE_INTEREST { get; set; }
        public string CUMULATIVE_ADMIN { get; set; }
        public string PRINCIPAL_BALANCE { get; set; }
    }

    public class cvs
    {
        public string product_code { get; set; }
        public String Product_name { get; set; }
    }
    public class Cust
    {
        public string SURNAME { get; set; }
        public string FORENAMES { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string CURR_EMPLOYER { get; set; }
        public DateTime? CURR_EMP_DATE { get; set; }
        public string PHONE_NO { get; set; }
        public string ADDRESS { get; set; }
    }
    public class GD
    {

        public string Shares { get; set; }
        public string CURR_EMPLOYER { get; set; }
        public string CURR_EMP_ADD { get; set; }
        public string CURR_EMP_DATE { get; set; }
        public string CURR_EMP_PHONE { get; set; }
        public string CURR_EMP_EMAIL { get; set; }
    }

    public class ProfileUpdate
    {
        [Required]
        [DisplayName("Surname")]
        public string SURNAME { get; set; }
        [Required]
        [DisplayName("First Name")]
        public string FORENAMES { get; set; }
        [Required]
        [DisplayName("Date Of Birth")]
        public string DOB { get; set; }
        [Required]
        [DisplayName("iD Number")]
        public string IDNO { get; set; }
        [Required]
        [DisplayName("City")]
        public string CITY { get; set; }
        [Required]
        [DisplayName("Mobile")]
        public string PHONE_NO { get; set; }
        [Required]
        [DisplayName("Nationality")]
        public string NATIONALITY { get; set; }
        [Required]
        [DisplayName("Gender")]
        public string GENDER { get; set; }
        [Required]
        [DisplayName("Marital Status")]
        public string MARITAL_STATUS { get; set; }
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Current Employer")]
        public string CURR_EMPLOYER { get; set; }
        [Required]
        [DisplayName("Curremt Employer Address")]
        public string CURR_EMP_ADD { get; set; }
        [Required]
        [DisplayName("Current Employer Phone")]
        public string CURR_EMP_PHONE { get; set; }
        [Required]
        [DisplayName("Current Employer Email")]
        public string CURR_EMP_EMAIL { get; set; }
        [Required]
        [DisplayName("Current Employer Salary")]
        public string CURR_EMP_SALARY { get; set; }
        [Required]
        [DisplayName("Current Employer Net")]
        public string CURR_EMP_NET { get; set; }
        [Required]
        [DisplayName("Employment Type")]
        public string EmployType { get; set; }
    }
   

}