﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodFellowWeb.Models
{
    public class Posting
    {
        [Required]
        public DateTime From { get; set; }
        [Required]
        public DateTime To { get; set; }
        [Required]
        public string company { get; set; }
    }
    public class Portfolio
    {
        public string Security { get; set; }

    }

    public class Registration
    {
        public string idnumber { get; set; }
        public string othername { get; set; }
        public string surname { get; set; }
        public string postalcode { get; set; }
        public string country { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string nationality { get; set; }
        public string resident { get; set; }
        public string phone { get; set; }
    }
    public class PostingL
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public List<Accountss_Master> Questions { set; get; }
        public PostingL()
        {
            Questions = new List<Accountss_Master>();
        }
    }

    public class Accountss_Master
    {

        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
    }
    public class PostingD
    {
        [Required]
        public DateTime From { get; set; }
        [Required]
        public DateTime To { get; set; }
        [Required]
        [DisplayName("Date Type")]
        public string DT { get; set; }
    }

    public class PostingW
    {
        [Required]
        public DateTime To { get; set; }
        [Required]
        [DisplayName("Date Type")]
        public string DT { get; set; }
    }
    public class Orders
    {

        public string Amount { get; set; }
        public string Security { get; set; }
    }
    public class Companies
    {

        public string Issuer_code { get; set; }
        public string Security_Description { get; set; }
    }
    public class PARA_BILLING
    {
        public string TranDate { get; set; }
        public string CDSNumber { get; set; }
        public string Company { get; set; }
        public string TranId { get; set; }
        public string Shares { get; set; }
    }

    public class ParaEvent
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Company { get; set; }
        public decimal Shares { get; set; }
        public string EventType { get; set; }
        public string Security_Description { get; set; }
        public string Id { get; set; }


    }

}