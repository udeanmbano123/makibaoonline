﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
namespace GwendNyadhi.Models
{
    public class familyPlanDependentsDB
    {
        public List<FamilyDependant> ListAll(string s)
        {
            List<FamilyDependant> lst = new List<FamilyDependant>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlanDDD/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyDependant>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        public List<FamilyDependant> getem(int s)
        {
            List<FamilyDependant> lst = new List<FamilyDependant>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("FamiyPlanDD/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s.ToString());
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<FamilyDependant>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        //Method for Adding an Employee
        public string Add(FamilyDependant my)
        {
            string CustNo = HttpContext.Current.User.Identity.Name; ;
            my.PlanNo = CustNo;
            string neme = isN(Convert.ToDateTime(my.DOB).ToString("dd-MMM-yyyy"));

            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("FamiyPlanD/{a}/{b}/{c}/{d}/{e}/{f}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.FullNames)));
            request.AddUrlSegment("b", enc(isN(my.Relationship)));
            request.AddUrlSegment("c", enc(isN(my.Email)));
            request.AddUrlSegment("d", enc(isN(my.Phone)));
            request.AddUrlSegment("e", enc(isN(my.PlanNo)));
            request.AddUrlSegment("f", enc(isN(neme)));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        //Method for Updating Employee record
        public int Update(FamilyDependant my)
        {
            string neme = isN(Convert.ToDateTime(my.DOB).ToString("dd-MMM-yyyy"));
            int i = 1;
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamiyPlanDU/{a}/{b}/{c}/{d}/{e}/{f}/{g}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.FullNames)));
            request.AddUrlSegment("b", enc(isN(my.Relationship)));
            request.AddUrlSegment("c", enc(isN(my.Email)));
            request.AddUrlSegment("d", enc(isN(my.Phone)));
            request.AddUrlSegment("e", enc(isN(my.PlanNo)));
            request.AddUrlSegment("f", enc(isN(neme)));
            request.AddUrlSegment("g", enc(isN(my.id.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return i;
        }
        //Method for Deleting an Employee
        public string Delete(int ID)
        {
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("FamiyPlanD/{a}", Method.DELETE);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", ID.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}