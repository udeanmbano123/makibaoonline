﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodFellowWeb.Models
{
    public class saccoNomineeDB
    {
        public List<sacco_NEXTKIN> ListAll(string s)
        {
            List<sacco_NEXTKIN> lst = new List<sacco_NEXTKIN>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("nextNL/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s);
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<sacco_NEXTKIN>>(response.Content);

            }
            catch (Exception)
            {

             
            } return lst;
        }
        public List<sacco_NEXTKIN> getem(int s)
        {
            List<sacco_NEXTKIN> lst = new List<sacco_NEXTKIN>();
            try
            {
                var client = new RestClient("http://localhost/CMSAppG/");
                var request = new RestRequest("nextNLU/{a}", Method.GET);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("a", s.ToString());
                IRestResponse response = client.Execute(request);
                lst = JsonConvert.DeserializeObject<List<sacco_NEXTKIN>>(response.Content);

            }
            catch (Exception)
            {


            }
            return lst;
        }
        //Method for Adding an Employee
        public string Add(sacco_NEXTKIN my)
        {
            var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("nextN/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Surname)));
             request.AddUrlSegment("b", enc(isN(my.Name)));
            request.AddUrlSegment("c", enc(isN(my.OtherNames)));
            request.AddUrlSegment("d", enc(isN(my.PBox)));
             request.AddUrlSegment("e", enc(isN(my.PostalCode)));
            request.AddUrlSegment("f", enc(isN(my.ID_Number)));
            request.AddUrlSegment("g", enc(isN(my.Email)));
             request.AddUrlSegment("h", enc(isN(my.Phone)));
            request.AddUrlSegment("i", enc(isN(my.City)));
            request.AddUrlSegment("j", enc(isN(my.CustomerNo)));
            request.AddUrlSegment("k", enc(isN(my.Percentage.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        //Method for Updating Employee record
        public int Update(sacco_NEXTKIN my)
        {
            int i = 1;
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("nextNUpdate/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.Surname)));
            request.AddUrlSegment("b", enc(isN(my.Name)));
            request.AddUrlSegment("c", enc(isN(my.OtherNames)));
            request.AddUrlSegment("d", enc(isN(my.PBox)));
            request.AddUrlSegment("e", enc(isN(my.PostalCode)));
            request.AddUrlSegment("f", enc(isN(my.ID_Number)));
            request.AddUrlSegment("g", enc(isN(my.Email)));
            request.AddUrlSegment("h", enc(isN(my.Phone)));
            request.AddUrlSegment("i", enc(isN(my.City)));
            request.AddUrlSegment("j", enc(isN(my.CustomerNo)));
            request.AddUrlSegment("k", enc(isN(my.Percentage.ToString())));
            request.AddUrlSegment("l", my.id.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return i;
        }
        //Method for Deleting an Employee
        public string Delete(int ID)
        {
            var client = new RestClient("http://localhost/CMSAppG/");
            var request = new RestRequest("nextNR/{a}", Method.DELETE);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a",ID.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
             var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}