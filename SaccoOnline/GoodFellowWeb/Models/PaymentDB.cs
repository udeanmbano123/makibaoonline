﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using GoodFellowWeb.DAO.security;

namespace GwendNyathi.Models
{
    public class PaymentDB
    {
        public string Add(Payment my)
        {

            string CustNo = HttpContext.Current.User.Identity.Name;

            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("Payy/{CustomerNumber}/{Amount}/{PaymentType}/{MobileNumber}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            
             request.AddUrlSegment("CustomerNumber", enc(isN(my.CustomerNumber)));
            request.AddUrlSegment("Amount", enc(isN(my.Amount.ToString())));
            request.AddUrlSegment("PaymentType", enc(isN(my.PaymentType)));
            request.AddUrlSegment("MobileNumber", enc(isN(my.MobileNumber)));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}