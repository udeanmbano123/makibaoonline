﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using GoodFellowWeb.DAO.security;
using GwendNyadhi.Models;

namespace GwendNyathi.Models
{
    public class WithdrawalDB
    {
        public string SavB(string p)
        {

            string CustNo = HttpContext.Current.User.Identity.Name;

            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("Saving/{a}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a",CustNo);
         IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        public string ShvB(string p)
        {

            string CustNo = HttpContext.Current.User.Identity.Name;

            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("Shares/{a}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", CustNo);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }

        public string WB(Withdrawals my)
        {

            string CustNo = HttpContext.Current.User.Identity.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("Withdrawal/{CustomerNumber}/{SavingsAmount}/{Amount}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("CustomerNumber", enc(isN(CustNo)));
            request.AddUrlSegment("SavingsAmount", enc(isN(my.SavingsAmount.ToString())));
            request.AddUrlSegment("Amount", enc(isN(my.Amount.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;
            return val;
        }

        public string SH(ShareTransfer my)
        {
            string CustNo = HttpContext.Current.User.Identity.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("TransferShares/{CustomerNumber}/{ShareBalance}/{TransferTo}/{numbershares}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("CustomerNumber", enc(isN(CustNo)));
            request.AddUrlSegment("ShareBalance", enc(isN(my.ShareBalance.ToString())));
            request.AddUrlSegment("TransferTo", enc(isN(my.TransferTo.ToString())));
            request.AddUrlSegment("numbershares", enc(isN(my.numbershares.ToString())));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;
            return val;
        }
        public string PR(ProfileUpdate v)
        {
            string CustNo = HttpContext.Current.User.Identity.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("ProfileUpdateU/{CustomerNumber}/{SURNAME}/{FORENAMES}/{DOB}/{IDNO}/{CITY}/{PHONE_NO}/{NATIONALITY}/{GENDER}/{MARITAL_STATUS}/{Email}/{CURR_EMPLOYER}/{CURR_EMP_ADD}/{CURR_EMP_PHONE}/{CURR_EMP_EMAIL}/{CURR_EMP_SALARY}/{CURR_EMP_NET}/{EmployType}");
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("CustomerNumber", enc(isN(CustNo)));
            request.AddUrlSegment("SURNAME", enc(isN(v.SURNAME)));
            request.AddUrlSegment("FORENAMES", enc(isN(v.FORENAMES)));
            request.AddUrlSegment("DOB", enc(isN(v.DOB)));
            request.AddUrlSegment("IDNO", enc(isN(v.IDNO)));
            request.AddUrlSegment("CITY", enc(isN(v.CITY)));
            request.AddUrlSegment("PHONE_NO", enc(isN(v.PHONE_NO)));
            request.AddUrlSegment("NATIONALITY", enc(isN(v.NATIONALITY)));
            request.AddUrlSegment("GENDER", enc(isN(v.GENDER)));
            request.AddUrlSegment("MARITAL_STATUS", enc(isN(v.MARITAL_STATUS)));
            request.AddUrlSegment("Email", enc(isN(v.Email)));
            request.AddUrlSegment("CURR_EMPLOYER", enc(isN(v.CURR_EMPLOYER)));
            request.AddUrlSegment("CURR_EMP_ADD", enc(isN(v.CURR_EMP_ADD)));
            request.AddUrlSegment("CURR_EMP_PHONE", enc(isN(v.CURR_EMP_PHONE)));
            request.AddUrlSegment("CURR_EMP_EMAIL", enc(isN(v.CURR_EMP_EMAIL)));
            request.AddUrlSegment("CURR_EMP_SALARY", enc(isN(v.CURR_EMP_SALARY)));
            request.AddUrlSegment("CURR_EMP_NET", enc(isN(v.CURR_EMP_NET)));
            request.AddUrlSegment("EmployType", enc(isN(v.EmployType)));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            validate = validate.Replace(@"""", "");
            string val = validate;
            return val;
        }
        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }

    }
}