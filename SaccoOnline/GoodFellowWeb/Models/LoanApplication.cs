﻿using GoodFellowWeb.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using GoodFellowWeb.DAO.security;

namespace GwendNyadhi.Models
{
    public class LoanApplicationDB
    {
        public string Add(LoanApplication my)
        {

            string neme = isN(Convert.ToDateTime(my.DOB).ToString("dd-MMM-yyyy"));

            string neme2 = isN(Convert.ToDateTime(my.JobStartDate).ToString("dd-MMM-yyyy"));
            string CustNo = HttpContext.Current.User.Identity.Name; ;

            //var neme = my.Name;
            var client = new RestClient("http://localhost/CMSAppG");
            var request = new RestRequest("LoApp/{a}/{b}/{c}/{d}/{e}/{f}/{g}/{h}/{i}/{j}/{k}/{l}/m}/{n}/{o}/{p}/{q}/{r}/{s}/{t}/{u}", Method.POST);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", enc(isN(my.coapplicant)));
            request.AddUrlSegment("b", enc(isN(my.First)));
            request.AddUrlSegment("c", enc(isN(my.Last)));
            request.AddUrlSegment("d", enc(isN(neme)));
            request.AddUrlSegment("e", enc(isN(my.Phone)));
            request.AddUrlSegment("f", enc(isN(my.Address)));
            request.AddUrlSegment("g", enc(isN(my.OwnRent)));
            request.AddUrlSegment("h", enc(isN(my.MonthlyAmout)));
            request.AddUrlSegment("i", enc(isN(my.Email)));
            request.AddUrlSegment("j", enc(isN(my.Amount)));
            request.AddUrlSegment("k", enc(isN("Gwend Nyadhi Application")));
            request.AddUrlSegment("l", enc(isN(my.Savings.ToString())));
            request.AddUrlSegment("m", enc(isN(neme2)));
            request.AddUrlSegment("n", enc(isN(my.JobTitle)));
            request.AddUrlSegment("o", enc(isN(my.EmployerName)));
            request.AddUrlSegment("p", enc(isN(my.Hear)));
            request.AddUrlSegment("q", enc(isN(my.AnnualIncome.ToString())));
            request.AddUrlSegment("r", enc(CustNo));
            request.AddUrlSegment("s", enc(my.Tenor.ToString()));
            request.AddUrlSegment("t", enc(my.Product));
            request.AddUrlSegment("u", enc(System.Web.HttpContext.Current.Session["NN"].ToString()));
             IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;

            return val;
        }
        public string enc(string s)
        {
            byte[] buyte = Encoding.UTF8.GetBytes(s);
            //string mwa = Convert.ToBase64String(buyte);
            var mwa = Convert.ToBase64String(buyte);
            return mwa;
        }
        public string isN(string s)
        {
            string v = "";
            v = s;
            if (s == "")
            {
                v = "None";
            }
            if (s == null)
            {
                v = "None";
            }

            return v;

        }
    }
}