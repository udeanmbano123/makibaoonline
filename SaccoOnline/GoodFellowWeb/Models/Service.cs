﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GwendNyathi.DROID;

namespace GoodFellowWeb.Models
{
    public class Service
    {
        public string baseUrl = "http://localhost/CMSAppG";
        public DROID3Entities db = new DROID3Entities();

        public List<LCounts> loanC(string s)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("LoanN/{t}", Method.GET);
            request.AddUrlSegment("t",s.Replace("*", "-"));
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;


            List<LCounts> dataList = JsonConvert.DeserializeObject<List<LCounts>>(validate);
            return dataList;
        }

        public dynamic loanCN(string s)
        {
            var client = new RestClient(baseUrl);
            var request = new RestRequest("nameN/{t}", Method.GET);
            request.AddUrlSegment("t", s.Replace("*", "-"));
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            IRestResponse response = client.Execute(request);
            // request.AddBody(new tblMeetings {  });
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            try
            {
                validate = validate.Split(',')[0];
            }
            catch (Exception)
            {

              
            }
            return validate;
        }

        public  int theimg()
       { int id = 0;

            try
            {
                var userid = db.Users.ToList().Where(a => a.MemberNumber == HttpContext.Current.User.Identity.Name).OrderByDescending(a => a.UserId).FirstOrDefault();

                var mxid = db.Contents.ToList().Where(a => a.UserID == userid.UserId).FirstOrDefault();

                if (mxid.ID >= 0)
                {
                    id = mxid.ID;
                }
            }
            catch (Exception)
            {

           
            }
           return id;
       }
        
    }
}