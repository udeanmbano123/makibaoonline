﻿using GoodFellowWeb.DAO.security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace GoodFellowWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {

                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                    CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                    newUser.UserId = serializeModel.UserId;
                    newUser.FirstName = serializeModel.FirstName;
                    newUser.LastName = serializeModel.LastName;
                    newUser.CDSNumber = serializeModel.CDSNumber;
                    newUser.roles = serializeModel.roles;
                    
                    HttpContext.Current.User = newUser;
                }
                catch (Exception)
                {

                  
                }
            }
        }
        protected void Session_Start(Object sender, EventArgs e)
        {

            string status = "No";
            //add session variables
            HttpContext.Current.Session.Add("Status", status);
            HttpContext.Current.Session.Add("Msisdn", status);
            HttpContext.Current.Session.Add("NatId", status);
            HttpContext.Current.Session.Add("CdsNumber", status);
            HttpContext.Current.Session.Add("OtherNames", status);
            HttpContext.Current.Session.Add("Surname", status);
        }
    }
}
