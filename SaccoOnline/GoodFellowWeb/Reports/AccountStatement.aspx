﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AccountStatement.aspx.cs" Inherits="GwendNyathi.Reporting.AccountStatement" %>


<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="section__content section__content--p30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">Account Information</div>
                    <div class="card-body">
              
         <a class="btn btn-primary" href="../User/AccountStatement">
   Back To Search

</a>
       </div>
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="GwendNyathi.Reporting.XtraStat" Theme="Moderno" OnCacheReportDocument="ASPxDocumentViewer1_CacheReportDocument" OnRestoreReportDocumentFromCache="ASPxDocumentViewer1_RestoreReportDocumentFromCache"></dx:ASPxDocumentViewer>
  
                    </div>
                </div></div></div>
         </div>
                    </asp:content>
