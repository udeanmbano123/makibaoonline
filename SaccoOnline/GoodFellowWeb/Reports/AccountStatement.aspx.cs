﻿using DevExpress.XtraReports.UI;
using GoodFellowWeb.Models;
using GwendNyadhi.Models;
using GwendNyathi.Reporting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GwendNyathi.Reporting
{
    public partial class AccountStatement : System.Web.UI.Page
    {
        public string id = "";
        public string frmdate = "";
        public string todate = "";
        public string company = "";
     
        protected void Page_Load(object sender, EventArgs e)
        {
            id = HttpContext.Current.User.Identity.Name;

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];
                if (Session["report2"] != null)
                {
                    ASPxDocumentViewer1.Report = GetReport();
                    ASPxDocumentViewer1.DataBind();
                }
            }

            else if (IsPostBack == false)
            {




                frmdate = Request.QueryString["From"];
                todate = Request.QueryString["To"];
                company = Request.QueryString["company"];


                try
                {



                    
                        ASPxDocumentViewer1.Report = GetReport();
                    Session["report2"] = GetReport();
                    ASPxDocumentViewer1.DataBind();
                  
                 

                }
                catch (Exception)
                {

                }
            }
        }
    
        XtraStatt GetReport()
        {
            XtraStatt report = new XtraStatt();

            report.Parameters["FDate"].Value = System.Web.HttpContext.Current.Session["From"].ToString();
            report.Parameters["TDate"].Value = System.Web.HttpContext.Current.Session["To"].ToString();
            report.Parameters["Account"].Value = id;
            report.Parameters["AccountName"].Value = System.Web.HttpContext.Current.Session["Surname"].ToString() + " " + System.Web.HttpContext.Current.Session["OtherNames"].ToString();
            report.Parameters["Address"].Value = "100000";
            report.Parameters["Total"].Value = GetBalance(System.Web.HttpContext.Current.Session["CdsNumber"].ToString(), System.Web.HttpContext.Current.Session["From"].ToString(), System.Web.HttpContext.Current.Session["To"].ToString(), System.Web.HttpContext.Current.Session["Company"].ToString());
            report.DataSource = GetData(System.Web.HttpContext.Current.Session["CdsNumber"].ToString(), System.Web.HttpContext.Current.Session["From"].ToString(), System.Web.HttpContext.Current.Session["To"].ToString(), System.Web.HttpContext.Current.Session["Company"].ToString());
          
            report.RequestParameters = false;
            return report;
        }

        void report_DataSourceDemanded(object sender, EventArgs e)
        {
            XtraReport report = sender as XtraReport;

          
            report.DataSource = GetData(System.Web.HttpContext.Current.Session["CdsNumber"].ToString(),frmdate,todate,company);
        }
        List<Data> GetData(string cdsnumber,string fromdate,string todate,string company)
        {
            List<Data> data = new List<Data>();
            double? totals = 0.0;
            var client = new RestClient(string.Format("http://192.168.3.14:9011/GetPortfolioStatement?cdsnumber={0}&startdate={1}&enddate={2}&company={3}",cdsnumber,fromdate,todate,company));
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            var validate = response.Content;
            var jsonData = JObject.Parse(validate);
            foreach (var x in jsonData)
            {

                if (x.Key == "PARA_BILLING")
                {
                    List<PARA_BILLING> dataList = JsonConvert.DeserializeObject<List<PARA_BILLING>>(x.Value.ToString());
                    foreach (var trans in dataList)
                    {
                        try
                        {
                            Data item = new Data();
                            item.tranDate = Convert.ToDateTime(trans.TranDate);
                            item.CDSNumber = trans.CDSNumber;
                            item.Company = trans.Company;
                            item.TranId = trans.TranId;

                            item.Shares = Convert.ToDouble(trans.Shares);
                            totals += item.shares;
                            item.runningshares = totals;
                           

                            data.Add(item);
                        }
                        catch (Exception)
                        {

                         
                        }
                        
                    }
                }
            }
            return data;
        }
        public string  GetBalance(string cdsnumber, string fromdate, string todate, string company)
        {
            List<Data> data = new List<Data>();
            double? totals = 0;
            var client = new RestClient(string.Format("http://192.168.3.14:9011/GetPortfolioStatement?cdsnumber={0}&startdate={1}&enddate={2}&company={3}", cdsnumber, fromdate, todate, company));
            var request = new RestRequest("", Method.GET);
            IRestResponse response = client.Execute(request);
            var validate = response.Content;
            var jsonData = JObject.Parse(validate);
            foreach (var x in jsonData)
            {

                if (x.Key == "PARA_BILLING")
                {
                    List<PARA_BILLING> dataList = JsonConvert.DeserializeObject<List<PARA_BILLING>>(x.Value.ToString());
                    foreach (var trans in dataList)
                    {
                       
                            Data item = new Data();
                        
                            item.Shares = Convert.ToDouble(trans.Shares);
                            totals += item.shares;
                            item.runningshares = totals;
                       
                      

                    }
                }
            }
            double total = Convert.ToDouble(totals);
            return total.ToString("#,##0.00");
        }

        protected void ASPxDocumentViewer1_CacheReportDocument(object sender, DevExpress.XtraReports.Web.CacheReportDocumentEventArgs e)
        {
            
               //Page.Session["report"] = e.SaveDocumentToMemoryStream();
        }

        protected void ASPxDocumentViewer1_RestoreReportDocumentFromCache(object sender, DevExpress.XtraReports.Web.RestoreReportDocumentFromCacheEventArgs e)
        {
            Stream stream = Page.Session["report"] as Stream;
            //if (stream != null)
              //  e.RestoreDocumentFromStream(stream);
        }
    }
}