var App = angular.module('validationApp', []);

App.controller('RegisterController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
  $scope.makibaLocalIp = "http://192.168.3.14:9011/";
    $rootScope.imessage = "Help";
    $scope.makibaIp ="http://192.168.3.14/APIMAKIBA/Subscriber/";

    $scope.submitForm = function (isValid) {
        $scope.isDisabled = false;
        if ($scope.Surname == "" || $scope.Surname == undefined || $scope.Surname == null) {
            $rootScope.Surnameerror = "Surname is required";
        } else if ($scope.OtherNames == "" || $scope.OtherNames == undefined || $scope.OtherNames == null) {
            $rootScope.OtherNameserror = "Other Names is required";
            $rootScope.Surnameerror = "";
        }  else if ($scope.IDNumber == "" || $scope.IDNumber == undefined || $scope.IDNumber == null) {
            $rootScope.IDNumbererror = "ID number is required";
            $rootScope.OtherNameserror = "";
        } else if ($scope.TelephoneNumber == "" || $scope.TelephoneNumber == undefined || $scope.TelephoneNumber == null || $scope.TelephoneNumber.length<12) {
            $rootScope.TelephoneNumbererror = "Telephone Number is required,formart(254779627951)";
            $rootScope.IDNumbererror = "";
        } else if ($scope.DOB == "" || $scope.DOB == undefined || $scope.DOB == null) {
            $rootScope.DOBerror = "Date Of Birth is required";
            $rootScope.TelephoneNumbererror = "";
        } else if ($scope.Country == "" || $scope.Country == undefined || $scope.Country == null) {
            $rootScope.Countryerror = "Country is required";
            $rootScope.DOBerror = "";
        } else if ($scope.Nationality == "" || $scope.Nationality == undefined || $scope.Nationality == null) {
            $rootScope.Nationalityerror = "Nationality is required";
            $rootScope.Countryerror = "";
        } else if ($scope.PostalCode == "" || $scope.PostalCode == undefined || $scope.PostalCode == null) {
            $rootScope.PostalCodeerror = "Postal Code is required";
            $rootScope.Nationalityerror = "";
        } else if ($scope.gender == "" || $scope.gender == undefined || $scope.gender == null) {
            $rootScope.gendererror = "Gender is required";
            $rootScope.PostalCodeerror = "";
        } else if ($scope.resident == "" || $scope.resident == undefined || $scope.resident == null) {
            $rootScope.residenterror = "Resident is required";
            $rootScope.gendererror = "";
        }  else {
            // check to make sure the form is completely valid
            $rootScope.residenterror = "";
            var d = new Date($scope.DOB);
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1; //Months are zero based
            var curr_year = d.getFullYear();
            str = curr_year + "-" + curr_month + "-" + curr_date;
            console.log($scope.makibaLocalIp + "NewClients?idnumber=" + $scope.IDNumber + "&othername=" + $scope.OtherNames + "&surname=" + $scope.Surname + "&postalcode=" + $scope.PostalCode + "&country=" + $scope.IDNumber + "&dob=" + str + "&gender=" + $scope.gender + "&nationality=" + $scope.Nationality + "&resident=" + $scope.resident + "&phone=" + $scope.TelephoneNumber)

            $scope.data = {

                idnumber: $scope.IDNumber,
                othername: $scope.OtherNames,
                surname: $scope.Surname ,
                postalcode: $scope.PostalCode ,
                country: $scope.Country ,
                dob: str,
                gender: $scope.OtherNames ,
                nationality: $scope.Nationality ,
                resident: $scope.resident ,
                phone: $scope.TelephoneNumber

            };
            var post = $http({
                method: "POST",
                url: "./Reg",
                dataType: 'json',
                data: { Registration: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                var datas = data.replace(/["']/g, "");
                //console.log(data);
                $scope.IDNumber = "";
                $scope.OtherNames = "";
                $scope.Surname = "";
                $scope.PostalCode = "";
                $scope.Country = "";
                $scope.OtherNames = "";
                $scope.Nationality = "";
                $scope.resident = "";
                $scope.TelephoneNumber = "";

                $rootScope.imessage = data.replace(/["']/g, "");
                   console.log($rootScope.imessage);
                   document.getElementById("openModalButton").click();
             

            });
               

               

            }
     
    };
   
});

App.controller('PrimaryMarketController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
    $scope.makibaLocalIp = "http://192.168.3.14:9011/";
    $rootScope.imessage = "Help";
    $scope.makibaIp = "http://192.168.3.14/APIMAKIBA/Subscriber/";
    $scope.securities = [];
    $http.get("../User/Companies")
        .then(function (response) {
            console.log("The data" + response.data);
            $scope.securities = response.data;
        });
    $scope.myFunc = function () {
        $scope.data = {

            Security: $scope.security

        };
        console.log("Portfoliio");
        var post = $http({
            method: "POST",
            url: "./Portfolios",
            dataType: 'json',
            data: { Portfolio: $scope.data },
            headers: { "Content-Type": "application/json" }
        });

        post.success(function (data, status) {
            $rootScope.portfolioresponse = data.replace(/["']/g, "");

        });
    };
    $scope.submitForm = function (isValid) {
        $scope.isDisabled = false;
        if ($scope.security == "" || $scope.security == undefined || $scope.security == null || $scope.security =="Select Security") {
            $rootScope.Securityerror = "Security is required";
        }else  if ($scope.Amount == 0 || $scope.Amount == null || $scope.Amount == undefined || $scope.Amount == "" || $scope.Amount.length == 0)
  {            $rootScope.Amounterror = "Amount is required";
            $rootScope.Securityerror = "";
        }  else {
            // check to make sure the form is completely valid

            $scope.data = {

                Amount: $scope.Amount,
                Security: $scope.security

            };
            var post = $http({
                method: "POST",
                url: "./PrimaryBuy",
                dataType: 'json',
                data: { Orders: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                var datas = data.replace(/["']/g, "");
                //console.log(data);

                alert(datas);

            });



        }

    };

});

App.controller('SecondaryBuyController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
    $scope.makibaLocalIp = "http://192.168.3.14:9011/";
    $rootScope.imessage = "Help";
    $scope.makibaIp = "http://192.168.3.14/APIMAKIBA/Subscriber/";
    $http.get("../User/Companies")
        .then(function (response) {
            $scope.securities = response.data;
        });
    $scope.myFunc = function () {
        $scope.data = {

            Security: $scope.security

        };
        console.log("Portfoliio");
        var post = $http({
            method: "POST",
            url: "./Portfolios",
            dataType: 'json',
            data: { Portfolio: $scope.data },
            headers: { "Content-Type": "application/json" }
        });

        post.success(function (data, status) {
            $rootScope.portfolioresponse = data.replace(/["']/g, "");

        });
    };
    $scope.submitForm = function (isValid) {
        $scope.isDisabled = false;
        if ($scope.security == "" || $scope.security == undefined || $scope.security == null || $scope.security == "Select Security")  {
            $rootScope.Securityerror = "Security is required";
        } else if ($scope.Amount == 0 || $scope.Amount == null || $scope.Amount == undefined || $scope.Amount == "" || $scope.Amount.length == 0) {
        $rootScope.Amounterror = "Amount is required";
            $rootScope.Securityerror = "";
        } else {
            // check to make sure the form is completely valid
            $scope.data = {

                Amount: $scope.Amount,
                Security: $scope.security

            };
            var post = $http({
                method: "POST",
                url: "./SecondaryBuys",
                dataType: 'json',
                data: { Orders: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                var datas = data.replace(/["']/g, "");
               // console.log(data);

                alert(datas);

            });


        }

    };

});


App.controller('SecondarySellController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
    $scope.makibaLocalIp = "http://192.168.3.14:9011/";
    $rootScope.imessage = "Help";
    $scope.makibaIp = "http://192.168.3.14/APIMAKIBA/Subscriber/";
    $scope.securities = [];
    $http.get("../User/Companies")
        .then(function (response) {
            console.log("The data"+response.data[0]);
            $scope.securities = response.data;
        });
    $scope.myFunc = function () {
        $scope.data = {

            Security: $scope.security

        };
        console.log("Portfoliio");
        var post = $http({
            method: "POST",
            url: "./Portfolios",
            dataType: 'json',
            data: { Portfolio: $scope.data },
            headers: { "Content-Type": "application/json" }
        });

        post.success(function (data, status) {
            $rootScope.portfolioresponse = data.replace(/["']/g, "");

        });
    };
    $scope.submitForm = function (isValid) {
        $scope.isDisabled = false;
        if ($scope.security == "" || $scope.security == undefined || $scope.security == null || $scope.security == "Select Security")  {
            $rootScope.Securityerror = "Security is required";
        } else if ($scope.Amount == 0 || $scope.Amount == null || $scope.Amount == undefined || $scope.Amount == "" || $scope.Amount.length == 0) {
        $rootScope.Amounterror = "Amount is required";
            $rootScope.Securityerror = "";
        } else {
            // check to make sure the form is completely valid

            $scope.data = {

                Amount: $scope.Amount,
                Security: $scope.security

            };
            var post = $http({
                method: "POST",
                url: "./SecondarySells",
                dataType: 'json',
                data: { Orders: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                var datas = data.replace(/["']/g, "");
                //console.log(data);

                alert(datas);

            });


        }

    };

});

App.controller('PortfolioController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
    $scope.makibaLocalIp = "http://192.168.3.14:9011/";
    $rootScope.imessage = "Help";
    $scope.makibaIp = "http://192.168.3.14/APIMAKIBA/Subscriber/";
    $http.get("../User/Companies")
        .then(function (response) {
            $scope.securities = response.data;
        });
    $scope.submitForm = function (isValid) {
        $scope.isDisabled = false;
        if ($scope.security == "" || $scope.security == undefined || $scope.security == null || $scope.security == "Select Security") {
            $rootScope.Securityerror = "Security is required";
        } else {
          
            $scope.data = {

                Security: $scope.security
               
            };
            var post = $http({
                method: "POST",
                url: "./Portfolios",
                dataType: 'json',
                data: { Portfolio: $scope.data },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function (data, status) {
                $rootScope.portfolioresponse= data.replace(/["']/g, "");

            });
        }

    };

});
App.controller('EventsController', function ($scope, $rootScope, $http, $location, $interval) {
    var base = $location.$$absUrl.replace($location.$$url, '');
    $scope.security = "Portfolio";
    $scope.data = {

        Security: $scope.security

    };

    var post = $http({
        method: "POST",
        url: "./GetEvents",
        dataType: 'json',
        data: { Portfolio: $scope.data },
        headers: { "Content-Type": "application/json" }
    });

    post.success(function (data, status) {
        $scope.eventslist = data;

    });

});



