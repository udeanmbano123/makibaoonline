app.controller('DepositCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data , $window) {
    $scope.list = [];
    $scope.searchButtonText_dep = " Make a Widthdrawal ► " ;
    $scope.pay_methd;
    $scope.pay_ammt;

    $scope.depositMoney = function () {

        if($scope.pay_ammt == 0 || $scope.pay_ammt == null || $scope.pay_ammt == undefined || $scope.pay_ammt == "" ||    $scope.pay_ammt.length == 0){
                alert("Please enter correct amount");                           
        }else{        
            $window.open('//ctrade.co.zw/api/PaynowPayments?cdsNumber=' + $sessionStorage.cds + '&price=' + $scope.pay_ammt + '&quantity=1&email=' + $sessionStorage.email); 
        }
        ngDialog.closeAll();

    };

    $scope.brokercz ; 
    Data.get('GetBrokers').then(function (results) {
        $scope.brokercz =  results ;
    });

    $scope.widthdrawMoney = function () {
        $scope.test = "true";
        $scope.searchButtonText = "Please Wait"; 
        if($scope.pay_ammt_with == 0 || $scope.pay_ammt_with == null || $scope.pay_ammt_with == undefined || $scope.pay_ammt_with == "" ||    $scope.pay_ammt_with.length == 0){
            alert("Please enter correct amount");                           
        }else{        
            Data.get('Widthdraw?cdsNumber='+ $sessionStorage.cds +'&ammount='+ $scope.pay_ammt_with).then(function (results) {
                alert(results) ; 
            });
        }


        ngDialog.closeAll();

    };
   
    $scope.changeBroker = function () {
        $sessionStorage.broker = $scope.change_broker ;
        Data.get('UpdateBroker?cdsnumber='+ $sessionStorage.cds +'&broker='+ $scope.change_broker).then(function (results) {
            alert(results) ; 
        });
        ngDialog.closeAll();

    };


});
