app.controller('dashboardCtrl', function ($scope, $rootScope,ngDialog, $interval ,$routeParams,$sessionStorage, $location, $http, Data, Idle, Keepalive) {
    //initially set those objects to null to avoid undefined error
    $scope.login = {};
    $scope.Login_Name = "Guest";
    $scope.countrx = 0 ; 
    $scope.paramValue = $location.search().message;  
    if ($sessionStorage.cds !== "") {
    	if($sessionStorage.cds !== undefined){
        	$scope.Login_Name = "logged";
    	}
    	else{
    		$scope.Login_Name = "Guest";
    	}
        // if($scope.countrx == 0 ){
        //     var paramValue = 
        //     alert("Hello mee " + paramValue + " - " + $scope.countrx ) ;
        //     $scope.countrx = $scope.countrx + 1   ; 
        // }
    };



    $scope.started = true;

    function closeModals() {
        if ($scope.warning) {
            $scope.warning.close();
            $scope.warning = null;
        }

        if ($scope.timedout) {
            $scope.timedout.close();
            $scope.timedout = null;
        }
    }

    $scope.$on('IdleStart', function() {
        closeModals();

        $scope.warning = ngDialog.open({
            template: 'warning-dialog.html',
            className: 'ngdialog-theme-default ngdialog-theme-custom modal-warning'
        });

    });

    $scope.$on('IdleEnd', function() {
        closeModals();
    });

    $scope.$on('IdleTimeout', function() {
        closeModals();
        $sessionStorage.$reset();
        window.location.reload();
        $location.path('/home');
       // alert("User is now logged out") ;
    });

    $scope.start = function() {
        closeModals();
        Idle.watch();
        $scope.started = true;
    };

    $scope.stop = function() {
        closeModals();
        Idle.unwatch();
        $scope.started = false;
    };

    $scope.fullname = $sessionStorage.username ; 
    $scope.email = $sessionStorage.email ; 
    $scope.cds = $sessionStorage.cds ;
    $scope.cash_balance  ; 
    $scope.v_cash_balance  ; 
    $scope.cash_transactions  ; 
    $scope.my_orders  ; 
    $scope.my_ipo_orders  ; 
    $scope.total_balance;
    $scope.portfolio_balance;
    $scope.pl_balance;
    $scope.companies_listing; 

    $scope.total = function(one , two) { 
        return parseFloat(one) * parseFloat(two) 
    }

    var updateClock = function() {
        //window.location.reload();
        Data.get('getCashBalance?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.cash_balance =  results[0].CashBal ;
            $scope.v_cash_balance =  results[0].VirtCashBal ;
            $scope.total_balance =  results[0].totalAccount ;
            $scope.portfolio_balance =  results[0].MyPotValue ;
            $scope.pl_balance =  results[0].MyProfitLoss ;
        });

        Data.get('CashTrans?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.cash_transactions =  results ;
        });

        Data.get('GetMyOrders?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_orders =  results ;
        });
        Data.get('GetMyIPOOrders?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_ipo_orders =  results ;
        });
        Data.get('getMyPortFolio?cdsNumber='+$scope.cds ).then(function (results) {
            $scope.my_portfolio =  results ;
        });        
    };

    $interval(updateClock, 60000);

    Data.get('getCashBalance?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.cash_balance =  results[0].CashBal ;
        $scope.v_cash_balance =  results[0].VirtCashBal ;
        $scope.total_balance =  results[0].totalAccount ;
        $scope.portfolio_balance =  results[0].MyPotValue ;
        $scope.pl_balance =  results[0].MyProfitLoss ;
    });

    Data.get('CashTrans?cdsNumber='+$scope.cds ).then(function (results) {
       $scope.cash_transactions =  results ;
       $scope.predicate = 'id';  
       $scope.reverse = true;  
       $scope.currentPage = 1;  
       $scope.order = function (predicate) {  
         $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;  
         $scope.predicate = predicate;  
       };  
       $scope.totalItems = $scope.cash_transactions.length;  
       $scope.numPerPage = 5;  
         
    });
       $scope.paginate = function (value) {  
         var begin, end, index;  
         begin = ($scope.currentPage - 1) * $scope.numPerPage;  
         end = begin + $scope.numPerPage;  
         index = $scope.cash_transactions.indexOf(value);  
         return (begin <= index && index < end);  
       };        
       $scope.paginatec = function (valuec) {  
         var beginc, endc, indexc;  
         beginc = ($scope.currentPagec - 1) * $scope.numPerPagec;  
         endc = beginc + $scope.numPerPagec;  
         indexc = $scope.companies_listing.indexOf(valuec);  
         return (beginc <= indexc && indexc < endc);  
       };     
       $scope.paginateo = function (valueo) {  
         var begino, endo, indexo;  
         begino = ($scope.currentPageo - 1) * $scope.numPerPageo;  
         endo = begino + $scope.numPerPageo;  
         indexo = $scope.my_orders.indexOf(valueo);  
         return (begino <= indexo && indexo < endo);  
       }; 
    Data.get('GetMyOrders?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_orders =  results ;
       $scope.predicateo = 'id';  
       $scope.reverseo = true;  
       $scope.currentPageo = 1;  
       $scope.ordero = function (predicateo) {  
         $scope.reverseo = ($scope.predicateo === predicateo) ? !$scope.reverseo : false;  
         $scope.predicateo = predicateo;  
       };  
       $scope.totalItemso = $scope.my_orders.length;  
       $scope.numPerPageo = 5;          
    });
    Data.get('getIPOISSUES' ).then(function (results) {
         $scope.ipo =  results ;
    });

    Data.get('GetMyIPOOrders?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_ipo_orders =  results ;
    });

    Data.get('getMyPortFolio?cdsNumber='+$scope.cds ).then(function (results) {
        $scope.my_portfolio =  results ;
    });

    Data.get('GetCompaniesListForGraphAll').then(function (results) {
        $scope.companies_listing =  results ;
       $scope.predicatec = 'Names';  
       $scope.reversec = true;  
       $scope.currentPagec = 1;  
       $scope.orderc = function (predicatec) {  
         $scope.reversec = ($scope.predicatec === predicatec) ? !$scope.reversec : false;  
         $scope.predicatec= predicatec;  
       };  
       $scope.totalItemsc = $scope.companies_listing.length;  
       $scope.numPerPagec = 10;  
    });


    $scope.logout = function () {
        $sessionStorage.$reset();
        alert("User is now logged out") ;
        window.location.reload();
        $location.path('/home');
    }
    $scope.graphs = function () {
        $location.path('/graph');
    }

    $scope.openOrderDiagBUY = function () {
        ngDialog.open({
            template: 'placeOrderDiagBUY',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };

    $scope.openOrderDiagBUYIPO = function () {
        ngDialog.open({
            template: 'placeOrderDiagBUYIPO',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };

    $scope.openDiagPerfomance = function () {
        ngDialog.open({
            template: 'performanceReport',
            className: 'ngdialog-theme-default ngdialog-theme-custom'            
        });
    };

    $scope.openChangeBroker = function () {
        ngDialog.open({
            template: 'placeOrderDiagChangeBroker',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };
    $scope.openOrderDiagSELL = function () {
        ngDialog.open({
            template: 'placeOrderDiagSELL',
            className: 'ngdialog-theme-default ngdialog-theme-custom'
        });
    };
    $scope.placeOrderDiagDeposit = function () {
        ngDialog.open({
            template: 'placeOrderDiagDeposit',
            className: 'ngdialog-theme-default ngdialog-theme-custom'           
        });
    };
    $scope.placeOrderDiagWidthdraw = function () {
        ngDialog.open({
            template: 'placeOrderDiagWidthdraw',
            className: 'ngdialog-theme-default ngdialog-theme-custom'           
        });
    };



});