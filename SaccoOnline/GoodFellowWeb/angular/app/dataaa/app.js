var app = angular.module('myApp', ['ngRoute','angular-encryption','ngResource',  'nvd3ChartDirectives', 'ngSanitize' , 'ui.bootstrap', 'ngAnimate', 'toaster', 'ngDialog' ,'ngStorage', 'nvd3' , 'ngIdle', 'tabs']);


app.config(['$routeProvider','IdleProvider', 'KeepaliveProvider',
  function ($routeProvider,IdleProvider, KeepaliveProvider) {
        IdleProvider.idle(600);
        IdleProvider.timeout(60);
        KeepaliveProvider.interval(60);
        $routeProvider.
        when('/home', {
            title: 'Home',
            templateUrl: 'templates/home_unlogged.html',
            controller: 'notLoggedCtrl'
        })
        .when('/logout', {
            title: 'Logout',
            templateUrl: 'partials/login.html',
            controller: 'logoutCtrl'
        })
        .when('/signup', {
            title: 'Signup',
            templateUrl: 'partials/signup.html',
            controller: 'authCtrl'
        })
        .when('/ipo', {
            title: 'IPO',
            templateUrl: 'templates/home_ipo_opening.html',
            controller: 'dashboardCtrl'
        })
        .when('/otp', {
            title: 'OTP',
            templateUrl: 'templates/home_logged_otp.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph', {
            title: 'Graphs',
            templateUrl: 'templates/home_graphs.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph_one', {
            title: 'Graphs',
            templateUrl: 'templates/home_graphs_1.html',
            controller: 'dashboardCtrl'
        })

        .when('/graph_two', {
            title: 'Graphs',
            templateUrl: 'templates/home_graphs_2x1.html',
            controller: 'dashboardCtrl'
        })
        .when('/graph_four', {
            title: 'Graphs',
            templateUrl: 'templates/home_graphs_2x2.html',
            controller: 'dashboardCtrl'
        })
        .when('/dashboard', {
            title: 'Dashboard',
            templateUrl: 'templates/home_logged.html',
            controller: 'dashboardCtrl'
            
        })
        .when('/', {
            title: 'Home',
            templateUrl: 'templates/home_unlogged.html',
            controller: 'notLoggedCtrl',
            role: '0'
        })
        .otherwise({
            redirectTo: '/home'
        });
  }]
  ).run(function ($rootScope, $location,$sessionStorage, Data) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $rootScope.authenticated = false;
            if($sessionStorage.id != null){
                $rootScope.id = $sessionStorage.id;
                $rootScope.username = $sessionStorage.username;
                $rootScope.email = $sessionStorage.email;
                $rootScope.broker = $sessionStorage.broker;

            }else{
                $location.path("/home");
            }
            // Data.getn('http://localhost/online.ctrade/api/session.php').then(function (results) {
            //     if (results.uid) {
            //         $rootScope.authenticated = true;
            //         $rootScope.id = results.id;
            //         $rootScope.username = results.username;
            //         $rootScope.email = results.email;
            //     } else {
            //         var nextUrl = next.$$route.originalPath;
            //         if (nextUrl == '/signup' || nextUrl == '/home') {

            //         } else {
            //             $location.path("/home");
            //         }
            //     }
            // });
        });
    });