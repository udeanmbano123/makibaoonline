app.controller('OrderIPOCtrl', function($scope, $rootScope, $sessionStorage, ngDialog, $location, $http, Data) {
    $scope.list = [];

    $scope.brokerz ; 
    //http://192.168.3.69/CTRADEAPI/subscriber/GetBrokers
    Data.get('GetBrokers').then(function (results) {
        $scope.brokerz =  results ;
    });

    $scope.company;
    $scope.security = "BOND";
    $scope.order_type;
    $scope.orderType = "Day";
    $scope.quantity;
    $scope.price;
    $scope.cdsNumber;
    $scope.broker;
    $scope.source = "online" ;
    $scope.cds = $sessionStorage.cds ;
    //http://localhost/CTRADEAPI/subscriber/GetBrokers
    $scope.doOrder = function (orderTyle) {
        // alert($scope.company) ;
        //http://localhost/CTRADEAPI/subscriber/OrderPosting?company=TESTCOMP1&security=EQUITY&orderTrans=BUY&orderType=Day&quantity=9800&price=1.1&cdsNumber=000000000103691&broker=WESTS
        Data.get('OrderPostingIPO?company='+$scope.company+'&security='+$scope.security+'&orderTrans='+orderTyle+'&orderType=Day&quantity='+$scope.quantity+'&price=1.0&cdsNumber='+$scope.cds+'&broker=AKRI&source='+$scope.source ).then(function (results) {
            // alert(results) ;
            $scope.company = "";
            $scope.security = "";
            $scope.order_type = "";
            $scope.quantity = "";
            $scope.price = "";
            $scope.cdsNumber = "";
            $scope.broker = "";
            ngDialog.closeAll();
            if(results == "1"){
                alert("Order successfully placed");
            }
            else {
                alert(results);
            }
        });

    };

    });
